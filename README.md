# README #

This is a tool mockup for the Google AdWords API. 
The "create_ads.py" script was used to create 50000 ad groups, text ads and keywords on the test account: luis@cybo.com.
With the Basic Access account level, the "create_ads.py" script was used to create and enable around 20,000 text ads on the account: cybo.global@gmail.com
The "create_ads.js" script was used to create 2000 ad groups, text ads and keywords on the test account luis@cybo.com. As of writing this we have around 80 impressions and 2 clicks from these ads.
The "create_ads_rmf_partial.py" script was only intended to be used to abide by Google's Required Minimum Functionality. It will not be used because we are applying for a token to be used internally, no third party will have access to this tool. Employees within Cybo are the only people who will have access to this tool.