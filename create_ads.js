// This script was used to create ads for the cybo.global@gmail account.
// It ran with the Bulk Operations feature in the Adwords API web console

var adgroups = {};
var keywords = {};

function getAlladGroups() {
  var adGroupIterator = AdWordsApp.adGroups().get();
  Logger.log('Total adGroups found : ' + adGroupIterator.totalNumEntities());
  while (adGroupIterator.hasNext()) {
    var adGroup = adGroupIterator.next();
    adgroups[adGroup.getName()] = true
  }
}

function getKeywordsInAdGroup(groupname) {
  var keywordIterator = AdWordsApp.keywords()
      .withCondition('AdGroupName = "' + groupname + '"')
      .get();
  if (keywordIterator.hasNext()) {
    while (keywordIterator.hasNext()) {
      var keyword = keywordIterator.next();
      keywords[keyword.getText()] = true;
    }
  }
}

function addAdGroup(bname) {
  Logger.log('New ad group: ' + bname);
  var campaignIterator = AdWordsApp.campaigns()
      .withCondition('Name = "Eugene Listings 2"')
      .get();
  if (campaignIterator.hasNext()) {
    var campaign = campaignIterator.next();
    var adGroupOperation = campaign.newAdGroupBuilder()
        .withName(bname)
        .withCpc(.02)
        .build();
    Logger.log('ad group successful')
  }
}

function setupAdParamsInAdGroup(bname, l) {
  Logger.log('creating ad');
  var adGroupIterator = AdWordsApp.adGroups()
      .withCondition('Name = "' + bname + '"')
      .get();
  var pars = {
      's': ((l.s) ? 'Rating ' + l.s + '/5' : ''),
      'c': ((l.c.length < 50) ? l.c : ''),
      'n': l.n,
      'a': l.a
   };
  var h2 = '';
  if (pars.n) {
      if (pars.n.length < 30) {
          h2 = pars.n;
          delete pars.n;
          if ((h2 + pars.a).length < 30) {
              h2 += ((h2) ? ', ': '') + pars.a;
              delete pars.a;
          }
      }
  }
  else {
      delete pars.n
  }
  if (!h2 && pars.a.length < 30) {
      h2 = pars.a;
      delete pars.a;
  }
  Logger.log('header2: ' + h2);
  var d = '';
  for (var p in pars) {
    if (pars[p] && d.length < 80) {
      d += pars[p] + ' : ';
    }
  }
  if (d.slice(-3) == ' : ') {
    d = d.slice(0, -3)
  }
  if (!d) {
      d = 'Get directions for ' + bname
  }
  d = d.slice(0, 80);
  Logger.log('description: ' + d);
  if (adGroupIterator.hasNext()) {
    adGroup = adGroupIterator.next();
    adGroup.newAd().expandedTextAdBuilder()
        .withHeadlinePart1(bname)
        .withHeadlinePart2(((h2) ? h2 : 'Eugene, Oregon'))
        .withPath1(l.u1)
        .withPath2(l.u2.slice(0, 15))
        .withDescription(d)
        .withFinalUrl(l.u)
        .build();
    Logger.log('ad successful');
  }
}

function clean(bname) {
  var cleanb = bname.replace(/[&+]/ig, ' and ');
  cleanb = cleanb.replace(/ LLC ?| INC | INC$| MD ?| LLP ?| CPA ?| co | co$| etc ?| RN ?| L\.?P\.? ?| LPC | ADC | ADC$/ig, '');
  cleanb = cleanb.replace(/[`~!@#$%^*()=\[\]{}:;'"\\/?<>,.]/g, '');
  cleanb = cleanb.replace(/[-_]/g, ' ');
  cleanb = cleanb.replace(/\s\s+/g, ' ');
  cleanb = cleanb.toLowerCase().trim();
  return cleanb
}

function addKeyword(bname, keyword, url) {
  var adGroupIterator = AdWordsApp.adGroups()
      .withCondition('Name = "' + bname + '"')
      .get();
  if (adGroupIterator.hasNext()) {
    var adGroup = adGroupIterator.next();
    adGroup.newKeywordBuilder()
        .withText(keyword)
        .withCpc(.02)                          // Optional
        .withFinalUrl(url) // Optional
        .build();
    Logger.log('keyword successful');
  }
}

var ls = {'RadioShack - Eugene, OR': {'n': '', 'a': 'Willamette Plaza', 'u': 'https://yellowpages.cybo.com/US-biz/radioshack-eugene-or', 'u2': 'radioshack-eugene-or', 'p': '+1 541-342-6197', 'c': 'Retail sale of computers, peripheral units, software and telecommunications equipment in specialized stores', 's': '2', 'u1': 'US-biz'}, 'Goldworks': {'n': 'Downtown', 'a': '169 East Broadway', 'u': 'https://bank.cybo.com/US-biz/goldworks_11L', 'u2': 'goldworks_11L', 'p': '+1 541-343-2298', 'c': 'Loan companies', 's': '4', 'u1': 'US-biz'}, 'Chabad of Eugene': {'n': '', 'a': '1330 East 20th Avenue', 'u': 'https://yellowpages.cybo.com/US-biz/chabad-of-eugene_1Z', 'u2': 'chabad-of-eugene_1Z', 'p': '+1 541-484-7665', 'c': 'Synagogues', 's': '5', 'u1': 'US-biz'}, 'Chao Pra Ya Thai Cuisine': {'n': 'Whiteaker', 'a': '580 Adams Street', 'u': 'https://restaurant.cybo.com/US-biz/chao-pra-ya-thai-cuisine', 'u2': 'chao-pra-ya-thai-cuisine', 'p': '+1 541-344-1706', 'c': 'Restaurants and mobile food service activities', 's': '4', 'u1': 'US-biz'}, 'South Eugene Barbers': {'n': 'South Hills', 'a': '384 East 40th Avenue', 'u': 'https://hair-salon.cybo.com/US-biz/south-eugene-barbers_19', 'u2': 'south-eugene-barbers_19', 'p': '+1 541-338-4995', 'c': 'Hairdressing and other beauty treatment', 's': '5', 'u1': 'US-biz'}, 'Apex Machinery': {'n': 'Whiteaker', 'a': '100 Polk Street', 'u': 'https://yellowpages.cybo.com/US-biz/apex-machinery', 'u2': 'apex-machinery', 'p': '+1 541-344-1971', 'c': 'Retail trade, except of motor vehicles and motorcycles', 's': '5', 'u1': 'US-biz'}, 'French Nails & Spa': {'n': 'Cal Young', 'a': '8 Oakway Center', 'u': 'https://hair-salon.cybo.com/US-biz/french-nails-&-spa_1e', 'u2': 'french-nails-&-spa_1e', 'p': '+1 541-342-3814', 'c': 'Hairdressing and other beauty treatment', 's': '4', 'u1': 'US-biz'}, 'Pure Romance By Christy Fickes': {'n': 'Bethel-Danebo', 'a': '4154 Marshall Avenue', 'u': 'https://clothing-store.cybo.com/US-biz/pure-romance-by-christy-fickes', 'u2': 'pure-romance-by-christy-fickes', 'p': '+1 541-680-2190', 'c': "Women's clothing", 's': '5', 'u1': 'US-biz'}, 'Crescendo Organic Spirits': {'n': 'Western', 'a': '4065 West 11th Avenue #47', 'u': 'https://liquor.cybo.com/US-biz/crescendo-organic-spirits', 'u2': 'crescendo-organic-spirits', 'p': '+1 458-201-8669', 'c': 'Retail sale of beverages in specialized stores', 's': '5', 'u1': 'US-biz'}, 'Willow Creek Academy': {'n': 'Western', 'a': '2370 Parliament Street', 'u': 'https://school.cybo.com/US-biz/willow-creek-academy_2f', 'u2': 'willow-creek-academy_2f', 'p': '+1 541-485-8488', 'c': 'Education', 's': '5', 'u1': 'US-biz'}, 'Walmart Connection Center': {'n': 'Western', 'a': '4550 West 11th Avenue', 'u': 'https://yellowpages.cybo.com/US-biz/walmart-connection-center_768G', 'u2': 'walmart-connection-center_768G', 'p': '+1 541-344-1325', 'c': 'Retail trade, except of motor vehicles and motorcycles', 's': '4', 'u1': 'US-biz'}};

function main() {
    Logger.log('Creating ads for ' + Object.keys(ls).length + ' businesses');
    getAlladGroups();
    for (var a in adgroups) {
        getKeywordsInAdGroup(a)
    }
    Logger.log('Total keywords found : ' + Object.keys(keywords).length);
    Logger.log('---------------------------------');
    for (var l in ls) {
      var kw = clean(l);
      if (((l in adgroups) != true) && ((kw in keywords) != true)) {
        addAdGroup(l);
        Logger.log('Created new ad group');
        setupAdParamsInAdGroup(l, ls[l]);
        Logger.log('Created new ad');
        addKeyword(l, kw, ls[l].u);
        Logger.log('Created keywords');
      }
      else {
        Logger.log('Duplicate keyword or ad group: ' + l);
      }
      Logger.log('---------------------------------');
    }
}