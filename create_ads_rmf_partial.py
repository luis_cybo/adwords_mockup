import csv
import datetime
import uuid
import ast
import json
import re
import time
from pprint import pprint

import suds
from tqdm import tqdm
from urllib.parse import urlparse
from googleads import adwords
from googleads import errors


'''
Most of thest funcionts will not run successfully, they were used to try to abide by the Required Minimum Functionality.
'''

ISIC_DICT = {
    1: 'Agriculture, forestry and fishing', 2: 'Crop and animal production, hunting and related service activities',
    3: 'Growing of non-perennial crops', 4: 'Growing of cereals (except rice), leguminous crops and oil seeds',
    5: 'Growing of rice', 6: 'Growing of vegetables and melons, roots and tubers', 7: 'Growing of sugar cane',
    8: 'Growing of tobacco', 9: 'Growing of fibre crops', 10: 'Growing of other non-perennial crops',
    11: 'Growing of perennial crops', 12: 'Growing of grapes', 13: 'Growing of tropical and subtropical fruits',
    14: 'Growing of citrus fruits', 15: 'Growing of pome fruits and stone fruits',
    16: 'Growing of other tree and bush fruits and nuts', 17: 'Growing of oleaginous fruits',
    18: 'Growing of beverage crops', 19: 'Growing of spices, aromatic, drug and pharmaceutical crops',
    20: 'Growing of other perennial crops', 21: 'Plant propagation', 22: 'Plant propagation', 23: 'Animal production',
    24: 'Raising of cattle and buffaloes', 25: 'Raising of horses and other equines',
    26: 'Raising of camels and camelids', 27: 'Raising of sheep and goats', 28: 'Raising of swine/pigs',
    29: 'Raising of poultry', 30: 'Raising of other animals', 31: 'Mixed farming', 32: 'Mixed farming',
    33: 'Support activities to agriculture and post-harvest crop activities',
    34: 'Support activities for crop production', 35: 'Support activities for animal production',
    36: 'Post-harvest crop activities', 37: 'Seed processing for propagation',
    38: 'Hunting, trapping and related service activities', 39: 'Hunting, trapping and related service activities',
    40: 'Forestry and logging', 41: 'Silviculture and other forestry activities',
    42: 'Silviculture and other forestry activities', 43: 'Logging', 44: 'Logging',
    45: 'Gathering of non-wood forest products', 46: 'Gathering of non-wood forest products',
    47: 'Support services to forestry', 48: 'Support services to forestry', 49: 'Fishing and aquaculture',
    50: 'Fishing', 51: 'Marine fishing', 52: 'Freshwater fishing', 53: 'Aquaculture', 54: 'Marine aquaculture',
    55: 'Freshwater aquaculture', 56: 'Mining and quarrying', 57: 'Mining of coal and lignite',
    58: 'Mining of hard coal', 59: 'Mining of hard coal', 60: 'Mining of lignite', 61: 'Mining of lignite',
    62: 'Extraction of crude petroleum and natural gas', 63: 'Extraction of crude petroleum',
    64: 'Extraction of crude petroleum', 65: 'Extraction of natural gas', 66: 'Extraction of natural gas',
    67: 'Mining of metal ores', 68: 'Mining of iron ores', 69: 'Mining of iron ores',
    70: 'Mining of non-ferrous metal ores', 71: 'Mining of uranium and thorium ores',
    72: 'Mining of other non-ferrous metal ores', 73: 'Other mining and quarrying',
    74: 'Quarrying of stone, sand and clay', 75: 'Quarrying of stone, sand and clay', 76: 'Mining and quarrying n.e.c.',
    77: 'Mining of chemical and fertilizer minerals', 78: 'Extraction of peat', 79: 'Extraction of salt',
    80: 'Other mining and quarrying n.e.c.', 81: 'Mining support service activities',
    82: 'Support activities for petroleum and natural gas extraction',
    83: 'Support activities for petroleum and natural gas extraction',
    84: 'Support activities for other mining and quarrying', 85: 'Support activities for other mining and quarrying',
    86: 'Manufacturing', 87: 'Manufacture of food products', 88: 'Processing and preserving of meat',
    89: 'Processing and preserving of meat', 90: 'Processing and preserving of fish, crustaceans and molluscs',
    91: 'Processing and preserving of fish, crustaceans and molluscs',
    92: 'Processing and preserving of fruit and vegetables', 93: 'Processing and preserving of fruit and vegetables',
    94: 'Manufacture of vegetable and animal oils and fats', 95: 'Manufacture of vegetable and animal oils and fats',
    96: 'Manufacture of dairy products', 97: 'Manufacture of dairy products',
    98: 'Manufacture of grain mill products, starches and starch products', 99: 'Manufacture of grain mill products',
    100: 'Manufacture of starches and starch products', 101: 'Manufacture of other food products',
    102: 'Manufacture of bakery products', 103: 'Manufacture of sugar',
    104: 'Manufacture of cocoa, chocolate and sugar confectionery',
    105: 'Manufacture of macaroni, noodles, couscous and similar farinaceous products',
    106: 'Manufacture of prepared meals and dishes', 107: 'Manufacture of other food products n.e.c.',
    108: 'Manufacture of prepared animal feeds', 109: 'Manufacture of prepared animal feeds',
    110: 'Manufacture of beverages', 111: 'Manufacture of beverages',
    112: 'Distilling, rectifying and blending of spirits', 113: 'Manufacture of wines',
    114: 'Manufacture of malt liquors and malt',
    115: 'Manufacture of soft drinks; production of mineral waters and other bottled waters',
    116: 'Manufacture of tobacco products', 117: 'Manufacture of tobacco products',
    118: 'Manufacture of tobacco products', 119: 'Manufacture of textiles',
    120: 'Spinning, weaving and finishing of textiles', 121: 'Preparation and spinning of textile fibres',
    122: 'Weaving of textiles', 123: 'Finishing of textiles', 124: 'Manufacture of other textiles',
    125: 'Manufacture of knitted and crocheted fabrics', 126: 'Manufacture of made-up textile articles, except apparel',
    127: 'Manufacture of carpets and rugs', 128: 'Manufacture of cordage, rope, twine and netting',
    129: 'Manufacture of other textiles n.e.c.', 130: 'Manufacture of wearing apparel',
    131: 'Manufacture of wearing apparel, except fur apparel',
    132: 'Manufacture of wearing apparel, except fur apparel', 133: 'Manufacture of articles of fur',
    134: 'Manufacture of articles of fur', 135: 'Manufacture of knitted and crocheted apparel',
    136: 'Manufacture of knitted and crocheted apparel', 137: 'Manufacture of leather and related products',
    138: 'Tanning and dressing of leather; manufacture of luggage, handbags, saddlery and harness; dressing and dyeing of fur',
    139: 'Tanning and dressing of leather; dressing and dyeing of fur',
    140: 'Manufacture of luggage, handbags and the like, saddlery and harness', 141: 'Manufacture of footwear',
    142: 'Manufacture of footwear',
    143: 'Manufacture of wood and of products of wood and cork, except furniture; manufacture of articles of straw and plaiting materials',
    144: 'Sawmilling and planing of wood', 145: 'Sawmilling and planing of wood',
    146: 'Manufacture of products of wood, cork, straw and plaiting materials',
    147: 'Manufacture of veneer sheets and wood-based panels', 148: "Manufacture of builders' carpentry and joinery",
    149: 'Manufacture of wooden containers',
    150: 'Manufacture of other products of wood; manufacture of articles of cork, straw and plaiting materials',
    151: 'Manufacture of paper and paper products', 152: 'Manufacture of paper and paper products',
    153: 'Manufacture of pulp, paper and paperboard',
    154: 'Manufacture of corrugated paper and paperboard and of containers of paper and paperboard',
    155: 'Manufacture of other articles of paper and paperboard', 156: 'Printing and reproduction of recorded media',
    157: 'Printing and service activities related to printing', 158: 'Printing',
    159: 'Service activities related to printing', 160: 'Reproduction of recorded media',
    161: 'Reproduction of recorded media', 162: 'Manufacture of coke and refined petroleum products',
    163: 'Manufacture of coke oven products', 164: 'Manufacture of coke oven products',
    165: 'Manufacture of refined petroleum products', 166: 'Manufacture of refined petroleum products',
    167: 'Manufacture of chemicals and chemical products',
    168: 'Manufacture of basic chemicals, fertilizers and nitrogen compounds, plastics and synthetic rubber in primary forms',
    169: 'Manufacture of basic chemicals', 170: 'Manufacture of fertilizers and nitrogen compounds',
    171: 'Manufacture of plastics and synthetic rubber in primary forms', 172: 'Manufacture of other chemical products',
    173: 'Manufacture of pesticides and other agrochemical products',
    174: 'Manufacture of paints, varnishes and similar coatings, printing ink and mastics',
    175: 'Manufacture of soap and detergents, cleaning and polishing preparations, perfumes and toilet preparations',
    176: 'Manufacture of other chemical products n.e.c.', 177: 'Manufacture of man-made fibres',
    178: 'Manufacture of man-made fibres',
    179: 'Manufacture of basic pharmaceutical products and pharmaceutical preparations',
    180: 'Manufacture of pharmaceuticals, medicinal chemical and botanical products',
    181: 'Manufacture of pharmaceuticals, medicinal chemical and botanical products',
    182: 'Manufacture of rubber and plastics products', 183: 'Manufacture of rubber products',
    184: 'Manufacture of rubber tyres and tubes; retreading and rebuilding of rubber tyres',
    185: 'Manufacture of other rubber products', 186: 'Manufacture of plastics products',
    187: 'Manufacture of plastics products', 188: 'Manufacture of other non-metallic mineral products',
    189: 'Manufacture of glass and glass products', 190: 'Manufacture of glass and glass products',
    191: 'Manufacture of non-metallic mineral products n.e.c.', 192: 'Manufacture of refractory products',
    193: 'Manufacture of clay building materials', 194: 'Manufacture of other porcelain and ceramic products',
    195: 'Manufacture of cement, lime and plaster', 196: 'Manufacture of articles of concrete, cement and plaster',
    197: 'Cutting, shaping and finishing of stone', 198: 'Manufacture of other non-metallic mineral products n.e.c.',
    199: 'Manufacture of basic metals', 200: 'Manufacture of basic iron and steel',
    201: 'Manufacture of basic iron and steel', 202: 'Manufacture of basic precious and other non-ferrous metals',
    203: 'Manufacture of basic precious and other non-ferrous metals', 204: 'Casting of metals',
    205: 'Casting of iron and steel', 206: 'Casting of non-ferrous metals',
    207: 'Manufacture of fabricated metal products, except machinery and equipment',
    208: 'Manufacture of structural metal products, tanks, reservoirs and steam generators',
    209: 'Manufacture of structural metal products', 210: 'Manufacture of tanks, reservoirs and containers of metal',
    211: 'Manufacture of steam generators, except central heating hot water boilers',
    212: 'Manufacture of weapons and ammunition', 213: 'Manufacture of weapons and ammunition',
    214: 'Manufacture of other fabricated metal products; metalworking service activities',
    215: 'Forging, pressing, stamping and roll-forming of metal; powder metallurgy',
    216: 'Treatment and coating of metals; machining', 217: 'Manufacture of cutlery, hand tools and general hardware',
    218: 'Manufacture of other fabricated metal products n.e.c.',
    219: 'Manufacture of computer, electronic and optical products',
    220: 'Manufacture of electronic components and boards', 221: 'Manufacture of electronic components and boards',
    222: 'Manufacture of computers and peripheral equipment', 223: 'Manufacture of computers and peripheral equipment',
    224: 'Manufacture of communication equipment', 225: 'Manufacture of communication equipment',
    226: 'Manufacture of consumer electronics', 227: 'Manufacture of consumer electronics',
    228: 'Manufacture of measuring, testing, navigating and control equipment; watches and clocks',
    229: 'Manufacture of measuring, testing, navigating and control equipment',
    230: 'Manufacture of watches and clocks',
    231: 'Manufacture of irradiation, electromedical and electrotherapeutic equipment',
    232: 'Manufacture of irradiation, electromedical and electrotherapeutic equipment',
    233: 'Manufacture of optical instruments and photographic equipment',
    234: 'Manufacture of optical instruments and photographic equipment',
    235: 'Manufacture of magnetic and optical media', 236: 'Manufacture of magnetic and optical media',
    237: 'Manufacture of electrical equipment',
    238: 'Manufacture of electric motors, generators, transformers and electricity distribution and control apparatus',
    239: 'Manufacture of electric motors, generators, transformers and electricity distribution and control apparatus',
    240: 'Manufacture of batteries and accumulators', 241: 'Manufacture of batteries and accumulators',
    242: 'Manufacture of wiring and wiring devices', 243: 'Manufacture of fibre optic cables',
    244: 'Manufacture of other electronic and electric wires and cables', 245: 'Manufacture of wiring devices',
    246: 'Manufacture of electric lighting equipment', 247: 'Manufacture of electric lighting equipment',
    248: 'Manufacture of domestic appliances', 249: 'Manufacture of domestic appliances',
    250: 'Manufacture of other electrical equipment', 251: 'Manufacture of other electrical equipment',
    252: 'Manufacture of machinery and equipment n.e.c.', 253: 'Manufacture of general-purpose machinery',
    254: 'Manufacture of engines and turbines, except aircraft, vehicle and cycle engines',
    255: 'Manufacture of fluid power equipment', 256: 'Manufacture of other pumps, compressors, taps and valves',
    257: 'Manufacture of bearings, gears, gearing and driving elements',
    258: 'Manufacture of ovens, furnaces and furnace burners', 259: 'Manufacture of lifting and handling equipment',
    260: 'Manufacture of office machinery and equipment (except computers and peripheral equipment)',
    261: 'Manufacture of power-driven hand tools', 262: 'Manufacture of other general-purpose machinery',
    263: 'Manufacture of special-purpose machinery', 264: 'Manufacture of agricultural and forestry machinery',
    265: 'Manufacture of metal-forming machinery and machine tools', 266: 'Manufacture of machinery for metallurgy',
    267: 'Manufacture of machinery for mining, quarrying and construction',
    268: 'Manufacture of machinery for food, beverage and tobacco processing',
    269: 'Manufacture of machinery for textile, apparel and leather production',
    270: 'Manufacture of other special-purpose machinery',
    271: 'Manufacture of motor vehicles, trailers and semi-trailers', 272: 'Manufacture of motor vehicles',
    273: 'Manufacture of motor vehicles',
    274: 'Manufacture of bodies (coachwork) for motor vehicles; manufacture of trailers and semi-trailers',
    275: 'Manufacture of bodies (coachwork) for motor vehicles; manufacture of trailers and semi-trailers',
    276: 'Manufacture of parts and accessories for motor vehicles',
    277: 'Manufacture of parts and accessories for motor vehicles', 278: 'Manufacture of other transport equipment',
    279: 'Building of ships and boats', 280: 'Building of ships and floating structures',
    281: 'Building of pleasure and sporting boats', 282: 'Manufacture of railway locomotives and rolling stock',
    283: 'Manufacture of railway locomotives and rolling stock',
    284: 'Manufacture of air and spacecraft and related machinery',
    285: 'Manufacture of air and spacecraft and related machinery', 286: 'Manufacture of military fighting vehicles',
    287: 'Manufacture of military fighting vehicles', 288: 'Manufacture of transport equipment n.e.c.',
    289: 'Manufacture of motorcycles', 290: 'Manufacture of bicycles and invalid carriages',
    291: 'Manufacture of other transport equipment n.e.c.', 292: 'Manufacture of furniture',
    293: 'Manufacture of furniture', 294: 'Manufacture of furniture', 295: 'Other manufacturing',
    296: 'Manufacture of jewellery, bijouterie and related articles',
    297: 'Manufacture of jewellery and related articles',
    298: 'Manufacture of imitation jewellery and related articles', 299: 'Manufacture of musical instruments',
    300: 'Manufacture of musical instruments', 301: 'Manufacture of sports goods', 302: 'Manufacture of sports goods',
    303: 'Manufacture of games and toys', 304: 'Manufacture of games and toys',
    305: 'Manufacture of medical and dental instruments and supplies',
    306: 'Manufacture of medical and dental instruments and supplies', 307: 'Other manufacturing n.e.c.',
    308: 'Other manufacturing n.e.c.', 309: 'Repair and installation of machinery and equipment',
    310: 'Repair of fabricated metal products, machinery and equipment', 311: 'Repair of fabricated metal products',
    312: 'Repair of machinery', 313: 'Repair of electronic and optical equipment',
    314: 'Repair of electrical equipment', 315: 'Repair of transport equipment, except motor vehicles',
    316: 'Repair of other equipment', 317: 'Installation of industrial machinery and equipment',
    318: 'Installation of industrial machinery and equipment',
    319: 'Electricity, gas, steam and air conditioning supply',
    320: 'Electricity, gas, steam and air conditioning supply',
    321: 'Electric power generation, transmission and distribution',
    322: 'Electric power generation, transmission and distribution',
    323: 'Manufacture of gas; distribution of gaseous fuels through mains',
    324: 'Manufacture of gas; distribution of gaseous fuels through mains', 325: 'Steam and air conditioning supply',
    326: 'Steam and air conditioning supply',
    327: 'Water supply; sewerage, waste management and remediation activities',
    328: 'Water collection, treatment and supply', 329: 'Water collection, treatment and supply',
    330: 'Water collection, treatment and supply', 331: 'Sewerage', 332: 'Sewerage', 333: 'Sewerage',
    334: 'Waste collection, treatment and disposal activities; materials recovery', 335: 'Waste collection',
    336: 'Collection of non-hazardous waste', 337: 'Collection of hazardous waste', 338: 'Waste treatment and disposal',
    339: 'Treatment and disposal of non-hazardous waste', 340: 'Treatment and disposal of hazardous waste',
    341: 'Materials recovery', 342: 'Materials recovery',
    343: 'Remediation activities and other waste management services',
    344: 'Remediation activities and other waste management services',
    345: 'Remediation activities and other waste management services', 346: 'Construction',
    347: 'Construction of buildings', 348: 'Construction of buildings', 349: 'Construction of buildings',
    350: 'Renovation of buildings', 351: 'Construction of houses', 352: 'General contractors',
    353: 'Building consultants', 354: 'Other construction services', 355: 'Renovation of houses',
    356: 'Metal construction', 357: 'Civil engineering', 358: 'Construction of roads and railways',
    359: 'Construction of roads and railways', 360: 'Construction of utility projects',
    361: 'Construction of utility projects', 362: 'Construction of other civil engineering projects',
    363: 'Construction of other civil engineering projects', 364: 'Specialized construction activities',
    365: 'Demolition and site preparation', 366: 'Demolition', 367: 'Site preparation',
    368: 'Electrical, plumbing and other construction installation activities', 369: 'Electrical installation',
    370: 'Plumbing, heat and air-conditioning installation', 371: 'Private security activities',
    372: 'Air-conditioning installation and repair', 373: 'Heating installation and repair',
    374: 'Plumbing installation and repair', 375: 'Gas installation and repair', 376: 'Other construction installation',
    377: 'Building completion and finishing', 378: 'Building completion and finishing',
    379: 'Painting and paint contractors', 380: 'Window, glass and door installation',
    381: 'Kitchen and bath cabinets and remodel', 382: 'Other specialized construction activities',
    383: 'Other specialized construction activities', 384: 'Pool and spa contractors', 385: 'Roofing contractor',
    386: 'Wholesale and retail trade; repair of motor vehicles and motorcycles',
    387: 'Wholesale and retail trade and repair of motor vehicles and motorcycles', 388: 'Sale of motor vehicles',
    389: 'Sale of motor vehicles', 390: 'Trucks and trailers', 391: 'Caravans, rvs and motor homes',
    392: 'New car dealers', 393: 'Used car dealers', 394: 'Maintenance and repair of motor vehicles',
    395: 'Maintenance and repair of motor vehicles', 396: 'Body repair', 397: 'Brake, muffler and transmission repair',
    398: 'Auto glass', 399: 'Car wash and detail', 400: 'Sale of motor vehicle parts and accessories',
    401: 'Sale of motor vehicle parts and accessories', 402: 'Tires and batteries',
    403: 'Sale, maintenance and repair of motorcycles and related parts and accessories',
    404: 'Sale, maintenance and repair of motorcycles and related parts and accessories', 405: 'Motorcycle dealers',
    406: 'Motorcycle repair', 407: 'Wholesale trade, except of motor vehicles and motorcycles',
    408: 'Wholesale on a fee or contract basis', 409: 'Wholesale on a fee or contract basis',
    410: 'Wholesale of agricultural raw materials and live animals',
    411: 'Wholesale of agricultural raw materials and live animals', 412: 'Wholesale of food, beverages and tobacco',
    413: 'Wholesale of food, beverages and tobacco', 414: 'Wholesale of household goods',
    415: 'Wholesale of textiles, clothing and footwear', 416: 'Wholesale of other household goods',
    417: 'Wholesale of machinery, equipment and supplies',
    418: 'Wholesale of computers, computer peripheral equipment and software',
    419: 'Wholesale of electronic and telecommunications equipment and parts',
    420: 'Retail sale of textiles in specialized stores',
    421: 'Wholesale of agricultural machinery, equipment and supplies',
    422: 'Wholesale of other machinery and equipment', 423: 'Wholesale construction machinery',
    424: 'Other specialized wholesale', 425: 'Wholesale of solid, liquid and gaseous fuels and related products',
    426: 'Wholesale of metals and metal ores',
    427: 'Wholesale of construction materials, hardware, plumbing and heating equipment and supplies',
    428: 'Wholesale of waste and scrap and other products n.e.c.', 429: 'Agricultural chemical wholesale',
    430: 'General chemical wholesale', 431: 'Non-specialized wholesale trade', 432: 'Non-specialized wholesale trade',
    433: 'Wholesale store', 434: 'Retail trade, except of motor vehicles and motorcycles',
    435: 'Retail sale in non-specialized stores',
    436: 'Retail sale in non-specialized stores with food, beverages or tobacco predominating', 437: 'Grocery store',
    438: 'Convenience stores', 439: 'Other retail sale in non-specialized stores',
    440: 'Retail sale of food, beverages and tobacco in specialized stores',
    441: 'Retail sale of food in specialized stores', 442: 'Butcher', 443: 'Seafood stores', 444: 'Donut shops',
    445: 'Fruits and vegetables', 446: 'Health foods and organic', 447: 'Bakeries', 448: 'Candy stores', 449: 'Delis',
    450: 'Cheese, dairy and eggs', 451: 'Ethnic food', 452: 'Farmers market',
    453: 'Retail sale of beverages in specialized stores', 454: 'Wine, beer and liquor stores',
    455: 'Retail sale of tobacco products in specialized stores',
    456: 'Retail sale of automotive fuel in specialized stores',
    457: 'Retail sale of automotive fuel in specialized stores',
    458: 'Retail sale of information and communications equipment in specialized stores',
    459: 'Retail sale of computers, peripheral units, software and telecommunications equipment in specialized stores',
    460: 'Computer stores', 461: 'Mobile phone stores',
    462: 'Retail sale of audio and video equipment in specialized stores',
    463: 'Retail sale of other household equipment in specialized stores',
    464: 'Retail sale of hardware, paints and glass in specialized stores',
    465: 'Retail sale of carpets, rugs, wall and floor coverings in specialized stores',
    466: 'Retail sale of electrical household appliances, furniture, lighting equipment and other household articles in specialized stores',
    467: 'Musical instruments', 468: 'Lighting stores', 469: 'Furniture stores', 470: 'Household appliances and goods',
    471: 'Mattress stores', 472: 'Framing', 473: 'Security systems, safes and vaults',
    474: 'Retail sale of cultural and recreation goods in specialized stores',
    475: 'Retail sale of books, newspapers and stationary in specialized stores',
    476: 'Office supplies and stationery stores', 477: 'Book stores', 478: 'News dealers and newsstands',
    479: 'Retail sale of music and video recordings in specialized stores',
    480: 'Retail sale of sporting equipment in specialized stores', 481: 'Bicycles', 482: 'Boats',
    483: 'Fishing and tackle shops', 484: 'Retail sale of games and toys in specialized stores',
    485: 'Retail sale of other goods in specialized stores',
    486: 'Retail sale of clothing, footwear and leather articles in specialized stores', 487: 'Bridal shops',
    488: 'Shoe stores', 489: "Men's clothing", 490: "Women's clothing", 491: 'Children and baby clothing',
    492: 'Clothing accessories',
    493: 'Retail sale of pharmaceutical and medical goods, cosmetic and toilet articles in specialized stores',
    494: 'Pharmacies and drug stores', 495: 'Beauty aids', 496: 'Other retail sale of new goods in specialized stores',
    497: 'Animal and pet stores', 498: 'Gifts, cards and party supplies', 499: 'Jewelers and watches',
    500: 'Art galleries', 501: 'Guns and ammunition', 502: 'Opticians and eyewear', 503: 'Florists',
    504: 'Nurseries and garden supplies', 505: 'Sex shops', 506: 'Hobby shop', 507: 'Farm and ranch', 508: 'Aircraft',
    509: 'Retail sale of second-hand goods', 510: 'Antiques', 511: 'Auctions',
    512: 'Retail sale via stalls and markets',
    513: 'Retail sale via stalls and markets of food, beverages and tobacco products',
    514: 'Retail sale via stalls and markets of textiles, clothing and footwear',
    515: 'Retail sale via stalls and markets of other goods', 516: 'Retail trade not in stores, stalls or markets',
    517: 'Retail sale via mail order houses or via Internet', 518: 'Other retail sale not in stores, stalls or markets',
    519: 'Transportation and storage', 520: 'Land transport and transport via pipelines', 521: 'Transport via railways',
    522: 'Passenger rail transport, interurban', 523: 'Freight rail transport', 524: 'Other land transport',
    525: 'Urban and suburban passenger land transport', 526: 'Other passenger land transport', 527: 'Taxis',
    528: 'Freight transport by road', 529: 'Moving companies', 530: 'Transport via pipeline',
    531: 'Transport via pipeline', 532: 'Water transport', 533: 'Sea and coastal water transport',
    534: 'Sea and coastal passenger water transport', 535: 'Sea and coastal freight water transport',
    536: 'Inland water transport', 537: 'Inland passenger water transport', 538: 'Inland freight water transport',
    539: 'Air transport', 540: 'Passenger air transport', 541: 'Passenger air transport', 542: 'Freight air transport',
    543: 'Freight air transport', 544: 'Warehousing and support activities for transportation',
    545: 'Warehousing and storage', 546: 'Warehousing and storage', 547: 'Self storage',
    548: 'Support activities for transportation', 549: 'Service activities incidental to land transportation',
    550: 'Towing and roadside assistance', 551: 'Parking lots and garages',
    552: 'Service activities incidental to water transportation',
    553: 'Service activities incidental to air transportation', 554: 'Cargo handling',
    555: 'Other transportation support activities', 556: 'Freight forwarding',
    557: 'Customs brokers and clearing agents', 558: 'Transportation logistics', 559: 'Postal and courier activities',
    560: 'Postal activities', 561: 'Postal activities', 562: 'Courier activities', 563: 'Courier activities',
    564: 'Accommodation and food service activities', 565: 'Accommodation', 566: 'Short term accommodation activities',
    567: 'Short term accommodation activities', 568: 'Hotels and motels', 569: 'Bed and breakfasts',
    570: 'Holiday homes, cabins and resorts', 571: 'Hostels',
    572: 'Camping grounds, recreational vehicle parks and trailer parks',
    573: 'Camping grounds, recreational vehicle parks and trailer parks', 574: 'Rv park', 575: 'Other accommodation',
    576: 'Other accommodation', 577: 'Food and beverage service activities',
    578: 'Restaurants and mobile food service activities', 579: 'Restaurants and mobile food service activities',
    580: 'Fast food restaurants', 581: 'Mediterranean restaurants', 582: 'Seafood restaurants', 583: 'Cafes',
    584: 'Vegetarian restaurants', 585: 'Ice cream and yogurt shops', 586: 'Breakfast and brunch', 587: 'Buffets',
    588: 'African restaurants', 589: 'American restaurants', 590: 'Argentine restaurants', 591: 'Burger restaurants',
    592: 'Caribbean restaurants', 593: 'Chinese restaurants', 594: 'Ethiopian restaurants', 595: 'French restaurants',
    596: 'Non-life insurance', 597: 'German restaurants', 598: 'Greek restaurants', 599: 'Halal restaurants',
    600: 'Indian restaurants', 601: 'Indonesian restaurants', 602: 'Italian restaurants', 603: 'Japanese restaurants',
    604: 'Korean restaurants', 605: 'Latin american restaurants', 606: 'Malaysian restaurants',
    607: 'Mexican restaurants', 608: 'Middle eastern restaurants', 609: 'Pizza', 610: 'Russian restaurants',
    611: 'Sandwich shops', 612: 'Scandinavian restaurants', 613: 'Soup restaurants', 614: 'South african restaurants',
    615: 'Spanish restaurants', 616: 'Steakhouses', 617: 'Sushi restaurants', 618: 'Tapas bars',
    619: 'Thai restaurants', 620: 'Turkish restaurants', 621: 'Vietnamese restaurants', 622: 'Internet cafes',
    623: 'Food delivery services', 624: 'Street vendors', 625: 'Event catering and other food service activities',
    626: 'Event catering', 627: 'Other food service activities', 628: 'Beverage serving activities',
    629: 'Beverage serving activities', 630: 'Bars, pubs and taverns', 631: 'Information and communication',
    632: 'Publishing activities', 633: 'Publishing of books, periodicals and other publishing activities',
    634: 'Book publishing', 635: 'Publishing of directories and mailing lists',
    636: 'Publishing of newspapers, journals and periodicals', 637: 'Other publishing activities',
    638: 'Software publishing', 639: 'Software publishing',
    640: 'Motion picture, video and television programme production, sound recording and music publishing activities',
    641: 'Motion picture, video and television programme activities',
    642: 'Motion picture, video and television programme production activities', 643: 'Home insurance',
    644: 'Motion picture, video and television programme post-production activities',
    645: 'Motion picture, video and television programme distribution activities',
    646: 'Motion picture projection activities', 647: 'Adult movie theater',
    648: 'Sound recording and music publishing activities', 649: 'Sound recording and music publishing activities',
    650: 'Programming and broadcasting activities', 651: 'Radio broadcasting', 652: 'Radio broadcasting',
    653: 'Television programming and broadcasting activities',
    654: 'Television programming and broadcasting activities', 655: 'Telecommunications',
    656: 'Wired telecommunications activities', 657: 'Wired telecommunications activities',
    658: 'Wireless telecommunications activities', 659: 'Wireless telecommunications activities',
    660: 'Satellite telecommunications activities', 661: 'Satellite telecommunications activities',
    662: 'Other telecommunications activities', 663: 'Other telecommunications activities',
    664: 'Computer programming, consultancy and related activities',
    665: 'Computer programming, consultancy and related activities', 666: 'Computer programming activities',
    667: 'Computer consultancy and computer facilities management activities',
    668: 'Other information technology and computer service activities', 669: 'Information service activities',
    670: 'Data processing, hosting and related activities; web portals',
    671: 'Data processing, hosting and related activities', 672: 'Web portals',
    673: 'Other information service activities', 674: 'News agency activities',
    675: 'Other information service activities n.e.c.', 676: 'Financial and insurance activities',
    677: 'Financial service activities, except insurance and pension funding', 678: 'Monetary intermediation',
    679: 'Central banking', 680: 'Other monetary intermediation', 681: 'Banks', 682: 'Credit unions', 683: "Atm's",
    684: 'Loan companies', 685: 'Activities of holding companies', 686: 'Activities of holding companies',
    687: 'Trusts, funds and similar financial entities', 688: 'Trusts, funds and similar financial entities',
    689: 'Other financial service activities, except insurance and pension funding activities',
    690: 'Financial leasing', 691: 'Other credit granting', 692: 'Pawn shops', 693: 'Bail bonds',
    694: 'Other financial service activities, except insurance and pension funding activities, n.e.c.',
    695: 'Insurance, reinsurance and pension funding, except compulsory social security', 696: 'Insurance',
    697: 'Life insurance', 698: 'Auto insurance', 699: 'Medical insurance', 700: 'Reinsurance', 701: 'Reinsurance',
    702: 'Pension funding', 703: 'Pension funding',
    704: 'Activities auxiliary to financial service and insurance activities',
    705: 'Activities auxiliary to financial service activities, except insurance and pension funding',
    706: 'Administration of financial markets', 707: 'Security and commodity contracts brokerage',
    708: 'Other activities auxiliary to financial service activities', 709: 'Mortgage companies',
    710: 'Activities auxiliary to insurance and pension funding', 711: 'Risk and damage evaluation',
    712: 'Activities of insurance agents and brokers',
    713: 'Other activities auxiliary to insurance and pension funding', 714: 'Fund management activities',
    715: 'Fund management activities', 716: 'Real estate activities', 717: 'Real estate activities',
    718: 'Real estate activities with own or leased property',
    719: 'Real estate activities with own or leased property', 720: 'Real estate activities on a fee or contract basis',
    721: 'Real estate activities on a fee or contract basis', 722: 'Apartments', 723: 'Commercial real estate',
    724: 'Professional, scientific and technical activities', 725: 'Legal and accounting activities',
    726: 'Legal activities', 727: 'Legal activities', 728: 'Corporate and business law', 729: 'Family law',
    730: 'Labor and employment law', 731: 'Personal injury and product liability law', 732: 'Notary',
    733: 'Accounting, bookkeeping and auditing activities; tax consultancy',
    734: 'Accounting, bookkeeping and auditing activities; tax consultancy',
    735: 'Activities of head offices; management consultancy activities', 736: 'Activities of head offices',
    737: 'Activities of head offices', 738: 'Management consultancy activities',
    739: 'Management consultancy activities', 740: 'Public relations and communications agencies',
    741: 'Marketing consulting services', 742: 'Lobbyists and political consultants',
    743: 'Financial consulting services',
    744: 'Architectural and engineering activities; technical testing and analysis',
    745: 'Architectural and engineering activities and related technical consultancy',
    746: 'Architectural and engineering activities and related technical consultancy', 747: 'Architects',
    748: 'Technical testing and analysis', 749: 'Technical testing and analysis',
    750: 'Scientific research and development',
    751: 'Research and experimental development on natural sciences and engineering',
    752: 'Research and experimental development on natural sciences and engineering',
    753: 'Research and experimental development on social sciences and humanities',
    754: 'Research and experimental development on social sciences and humanities',
    755: 'Advertising and market research', 756: 'Advertising', 757: 'Advertising',
    758: 'Market research and public opinion polling', 759: 'Market research and public opinion polling',
    760: 'Other professional, scientific and technical activities', 761: 'Specialized design activities',
    762: 'Specialized design activities', 763: 'Interior decorations', 764: 'Photographic activities',
    765: 'Photographic activities', 766: 'Other professional, scientific and technical activities n.e.c.',
    767: 'Other professional, scientific and technical activities n.e.c.', 768: 'Translation services and interpreters',
    769: 'Entertainment agencies', 770: 'Veterinary activities', 771: 'Veterinary activities',
    772: 'Veterinary activities', 773: 'Administrative and support service activities',
    774: 'Rental and leasing activities', 775: 'Renting and leasing of motor vehicles',
    776: 'Renting and leasing of motor vehicles', 777: 'Trucks and trailer rental', 778: 'Car rental',
    779: 'Renting and leasing of personal and household goods',
    780: 'Renting and leasing of recreational and sports goods', 781: 'Renting of video tapes and disks',
    782: 'Renting and leasing of other personal and household goods',
    783: 'Renting and leasing of other machinery, equipment and tangible goods',
    784: 'Renting and leasing of other machinery, equipment and tangible goods', 785: 'Leasing construction machinery',
    786: 'Audio visual', 787: 'Leasing of intellectual property and similar products, except copyrighted works',
    788: 'Leasing of intellectual property and similar products, except copyrighted works',
    789: 'Employment activities', 790: 'Activities of employment placement agencies',
    791: 'Activities of employment placement agencies', 792: 'Temporary employment agency activities',
    793: 'Temporary employment agency activities', 794: 'Other human resources provision',
    795: 'Other human resources provision',
    796: 'Travel agency, tour operator, reservation service and related activities',
    797: 'Travel agency and tour operator activities', 798: 'Travel agency activities', 799: 'Tour operator activities',
    800: 'Other reservation service and related activities', 801: 'Other reservation service and related activities',
    802: 'Security and investigation activities', 803: 'Private security activities', 804: 'Higher education',
    805: 'Security systems service activities', 806: 'Security systems service activities', 807: 'Locksmiths',
    808: 'Investigation activities', 809: 'Investigation activities',
    810: 'Services to buildings and landscape activities', 811: 'Combined facilities support activities',
    812: 'Combined facilities support activities', 813: 'Cleaning activities', 814: 'General cleaning of buildings',
    815: 'Other building and industrial cleaning activities', 816: 'Exterminators and pest control',
    817: 'Swimming pool cleaning and maintenance', 818: 'Landscape care and maintenance service activities',
    819: 'Landscape care and maintenance service activities',
    820: 'Office administrative, office support and other business support activities',
    821: 'Office administrative and support activities', 822: 'Combined office administrative service activities',
    823: 'Photocopying, document preparation and other specialized office support activities',
    824: 'Activities of call centres', 825: 'Activities of call centres',
    826: 'Organization of conventions and trade shows', 827: 'Organization of conventions and trade shows',
    828: 'Business support service activities n.e.c.', 829: 'Activities of collection agencies and credit bureaus',
    830: 'Packaging activities', 831: 'Other business support service activities n.e.c.',
    832: 'Public administration and defence; compulsory social security',
    833: 'Public administration and defence; compulsory social security',
    834: 'Administration of the State and the economic and social policy of the community',
    835: 'General public administration activities',
    836: 'Regulation of the activities of providing health care, education, cultural services and other social services, excluding social security',
    837: 'Regulation of and contribution to more efficient operation of businesses',
    838: 'Provision of services to the community as a whole', 839: 'Foreign affairs', 840: 'Embassies and consulates',
    841: 'Defence activities', 842: 'Public order and safety activities', 843: 'Firefighting and rescue',
    844: 'Police and law enforcement', 845: 'Courts of law', 846: 'Compulsory social security activities',
    847: 'Compulsory social security activities', 848: 'Education', 849: 'Education',
    850: 'Pre-primary and primary education', 851: 'Pre-primary and primary education',
    852: 'Preschools and kindergartens', 853: 'Primary and elementary schools', 854: 'Secondary education',
    855: 'General secondary education', 856: 'Technical and vocational secondary education',
    857: 'Cosmetology and beauty schools', 858: 'Higher education', 859: 'Other education',
    860: 'Sports and recreation education', 861: 'Cultural education', 862: 'Other education n.e.c.',
    863: 'Driving schools', 864: 'Computer training', 865: 'Flying schools', 866: 'Educational support activities',
    867: 'Educational support activities', 868: 'Human health and social work activities',
    869: 'Human health activities', 870: 'Hospital activities', 871: 'Hospital activities',
    872: 'Medical and dental practice activities', 873: 'Medical and dental practice activities', 874: 'Dentists',
    875: 'Orthodontists', 876: 'Allergists', 877: 'Arthritis specialists', 878: 'Birth and family planning',
    879: 'Cardiologists', 880: 'Dermatologists', 881: 'Plastic surgery', 882: 'Ophthalmologists',
    883: 'Gynecologists and obstetricians', 884: 'Hearing', 885: 'Neurologists', 886: 'Oncologists', 887: 'Orthopedics',
    888: 'Pediatricians', 889: 'Physicians and surgeons', 890: 'Radiologists', 891: 'Urologists',
    892: 'General practitioners', 893: 'Oral and maxillofacial surgery', 894: 'Medical clinic',
    895: 'Other human health activities', 896: 'Other human health activities', 897: 'Acupuncture',
    898: 'Speech therapists', 899: 'Medical laboratories', 900: 'Ambulances', 901: 'Chiropractors', 902: 'Homeopathy',
    903: 'Alternative medicine', 904: 'Physiotherapy', 905: 'Optometrists', 906: 'Psychiatrists and psychotherapists',
    907: 'Nurses', 908: 'Midwives', 909: 'Chiropodists and podiatrists', 910: 'Nutritionists',
    911: 'Residential care activities', 912: 'Residential nursing care facilities',
    913: 'Residential nursing care facilities',
    914: 'Residential care activities for mental retardation, mental health and substance abuse',
    915: 'Residential care activities for mental retardation, mental health and substance abuse',
    916: 'Residential care activities for the elderly and disabled',
    917: 'Residential care activities for the elderly and disabled', 918: 'Other residential care activities',
    919: 'Other residential care activities', 920: 'Social work activities without accommodation',
    921: 'Social work activities without accommodation for the elderly and disabled',
    922: 'Social work activities without accommodation for the elderly and disabled',
    923: 'Other social work activities without accommodation',
    924: 'Other social work activities without accommodation', 925: 'Child care and day care', 926: 'Charities',
    927: 'Arts, entertainment and recreation', 928: 'Creative, arts and entertainment activities',
    929: 'Creative, arts and entertainment activities', 930: 'Creative, arts and entertainment activities',
    931: 'Artists and musicians', 932: 'Event management', 933: 'Bands, orchestras and choirs',
    934: 'Concert halls and theaters', 935: 'Libraries, archives, museums and other cultural activities',
    936: 'Libraries, archives, museums and other cultural activities', 937: 'Library and archives activities',
    938: 'Museums activities and operation of historical sites and buildings',
    939: 'Botanical and zoological gardens and nature reserves activities', 940: 'Gambling and betting activities',
    941: 'Gambling and betting activities', 942: 'Gambling and betting activities',
    943: 'Sports activities and amusement and recreation activities', 944: 'Sports activities',
    945: 'Operation of sports facilities', 946: 'Golf courses', 947: 'Fitness centers', 948: 'Public swimming pools',
    949: 'Bowling', 950: 'Skating rinks', 951: 'Stadiums and arenas', 952: 'Activities of sports clubs',
    953: 'Other sports activities', 954: 'Other amusement and recreation activities',
    955: 'Activities of amusement parks and theme parks', 956: 'Other amusement and recreation activities n.e.c.',
    957: 'Arcades', 958: 'Dance clubs and discotheques', 959: 'Ski hills', 960: 'Public parks', 961: 'Playground',
    962: 'Strip clubs', 963: 'Other service activities', 964: 'Activities of membership organizations',
    965: 'Activities of business, employers and professional membership organizations',
    966: 'Activities of business and employers membership organizations',
    967: 'Activities of professional membership organizations', 968: 'Activities of trade unions',
    969: 'Activities of trade unions', 970: 'Activities of other membership organizations',
    971: 'Activities of religious organizations', 972: 'Churches', 973: 'Mosques', 974: 'Synagogues',
    975: 'Buddhist temple', 976: 'Hindu temple', 977: 'Activities of political organizations',
    978: 'Activities of other membership organizations n.e.c.',
    979: 'Repair of computers and personal and household goods', 980: 'Repair of computers and communication equipment',
    981: 'Repair of computers and peripheral equipment', 982: 'Repair of communication equipment',
    983: 'Repair of personal and household goods', 984: 'Repair of consumer electronics',
    985: 'Repair of household appliances and home and garden equipment', 986: 'Repair of footwear and leather goods',
    987: 'Repair of furniture and home furnishings', 988: 'Repair of other personal and household goods',
    989: 'Other personal service activities', 990: 'Other personal service activities',
    991: 'Washing and (dry-) cleaning of textile and fur products', 992: 'Carpet and rug cleaning',
    993: 'Hairdressing and other beauty treatment', 994: 'Beauty salons', 995: 'Barbers',
    996: 'Manicures and pedicures', 997: 'Funeral and related activities',
    998: 'Other personal service activities n.e.c.', 999: 'Astrologers and spiritualists', 1000: 'Escort services',
    1001: 'Dating services', 1002: 'Tattoo and body piercing', 1003: 'Massage', 1004: 'Pet grooming and boarding',
    1005: 'Tanning salons', 1006: 'Animal shelter', 1007: 'Day spa', 1008: 'Diet',
    1009: 'Activities of households as employers; undifferentiated goods- and services-producing activities of households for own use',
    1010: 'Activities of households as employers of domestic personnel',
    1011: 'Activities of households as employers of domestic personnel',
    1012: 'Activities of households as employers of domestic personnel',
    1013: 'Undifferentiated goods- and services-producing activities of private households for own use',
    1014: 'Undifferentiated goods-producing activities of private households for own use',
    1015: 'Undifferentiated goods-producing activities of private households for own use',
    1016: 'Undifferentiated service-producing activities of private households for own use',
    1017: 'Undifferentiated service-producing activities of private households for own use',
    1018: 'Activities of extraterritorial organizations and bodies',
    1019: 'Activities of extraterritorial organizations and bodies',
    1020: 'Activities of extraterritorial organizations and bodies',
    1021: 'Activities of extraterritorial organizations and bodies', 1022: 'Medical marijuana dispensary',
    1023: 'Recreational marijuana dispensary', 1024: 'Marijuana specialists'
}

ADSENSE_UNSUPPORTED_ISICS = {1024, 1023, 962, 1000, 1001, 1003, 940, 941, 942, 505, 958, 1022}
# pharmacy cats need certification
ADWORDS_UNSUPPORTED_ISICS = {19, 179, 180, 181, 493, 494}

RE_AND = re.compile(r'[&+]')
RE_JUNK = re.compile(
    r' LLC ? | INC | INC$ | MD ? | LLP ? | CPA ? | co | co$ | etc ? | RN ? | L\.?P\.? ? | LPC | ADC | ADC$')
RE_JUNK2 = re.compile(r'[`~!@#$%^*()=\[\]{}:;\'\"/?<>,.]')
RE_SPACE = re.compile(r'[-_]')
RE_SPACE2 = re.compile(r'\s\s+')

PLATFORMS = {
    'desktop': 30000,
    'mobile': 30001,
    'tablet': 30002
}


def micro(num):
    return round(num*1000000)


def get_campaigns(client):
    PAGE_SIZE = 100
    campaign_service = client.GetService('CampaignService', version='v201702')

    offset = 0
    selector = {
        'fields': ['Id', 'Name', 'Status'],
        'paging': {
            'startIndex': str(offset),
            'numberResults': str(PAGE_SIZE)
        }
    }

    more_pages = True
    while more_pages:
        page = campaign_service.get(selector)
        if 'entries' in page:
            for campaign in page['entries']:
                print('Campaign with id \'%s\', name \'%s\', and status \'%s\' was '
                      'found.' % (campaign['id'], campaign['name'],
                                  campaign['status']))
        else:
            print('No campaigns were found.')
        offset += PAGE_SIZE
        selector['paging']['startIndex'] = str(offset)
        more_pages = offset < int(page['totalNumEntries'])
        time.sleep(1)


def get_adgroups(client, cid):
    groups = set()
    PAGE_SIZE = 500
    ad_group_service = client.GetService('AdGroupService', version='v201702')
    offset = 0
    selector = {
        'fields': ['Id', 'Name', 'Status'],
        'predicates': [
            {
                'field': 'CampaignId',
                'operator': 'EQUALS',
                'values': [cid]
            }
        ],
        'paging': {
            'startIndex': str(offset),
            'numberResults': str(PAGE_SIZE)
        }
    }
    more_pages = True
    while more_pages:
        page = ad_group_service.get(selector)
        if 'entries' in page:
            for ad_group in page['entries']:
                groups.add(ad_group['name'])
                # print('Ad group with name \'%s\', id \'%s\' and status \'%s\' was '
                #       'found.' % (ad_group['name'], ad_group['id'],
                #                   ad_group['status']))
        else:
            print('No ad groups were found.')
        offset += PAGE_SIZE
        selector['paging']['startIndex'] = str(offset)
        more_pages = offset < int(page['totalNumEntries'])
    return groups


def avg_social_reviews(jdata):
    er = jdata.get('extra_ratings') or {}
    if er:
        s, n = 0.0, 0.0
        # google
        if 'val_g_revs' in er:
            tot_g = er.get('tot_g_revs') or 0
            n += tot_g
            s += tot_g * er.get('val_g_revs', 0)
        # fsq_rating
        if 'fsq_rating' in er:
            if er.get('fsq_rating') and er.get('fsq_num_reviews'):
                s += (er['fsq_rating'] / 2) * er['fsq_num_reviews']
                n += er['fsq_num_reviews']
        # yelp
        if 'yelp_rating' in er:
            n += 5
            s += er['yelp_rating'] * 5.0
        if s or n:
            return str(round((s / n)))
    return ''


def format_keyword(bname):
    newbname = RE_AND.sub(' and ', bname)
    newbname = RE_JUNK.sub('', newbname)
    newbname = RE_JUNK2.sub('', newbname)
    newbname = RE_SPACE.sub(' ', newbname)
    newbname = RE_SPACE2.sub(' ', newbname)
    newbname = newbname.lower().strip()
    return newbname


def create_description(bname, j):
    # pprint(j)
    hmax = 30
    dmax = 80
    cmax = 50
    desc = ''
    header2 = ''
    address = j['a']
    if j['n']:
        header2 += j['n']
        del j['n']
    else:
        del j['n']
    if len(header2) < hmax:
        if address and (len(header2 + address) + 2) < hmax:
            header2 += (', ' + address) if header2 else address
            del j['a']
    if j['s']:
        desc += 'Rating ' + j['s'] + '/5'
    if 0 < len(j['c']) < cmax:
        if len(desc + j['c']) < dmax:
            desc += (' : ' + j['c']) if desc else j['c']
    if 'n' in j:
        if j['n'] and (len(desc + j['n']) < dmax):
            desc += (' : ' + j['n']) if desc else j['n']
    if 'a' in j:
        if j['a'] and (len(desc + j['a']) < dmax):
            desc += (' : ' + j['a']) if desc else j['a']
    if not desc:
        if 0 < len(j['c']) <= dmax:
            desc = j['c']
        else:
            desc = 'Get directions for ' + bname
    return header2 or 'Eugene, Oregon', desc
    # print('Header2: ' + header2)
    # print('Description: ' + desc)
    # print('-' * 100)


def create_location_extensions(client, gmb_email_address, gmb_access_token, business_account_identifier=None):  # C.70
    placeholder_location = 7
    max_customer_feed_add_attempts = 10
    feed = {
        'name': 'GMB feed #%s' % uuid.uuid4(),
        'systemFeedGenerationData': {
            'xsi_type': 'PlacesLocationFeedData',
            'oAuthInfo': {
                'httpMethod': 'GET',
                'httpRequestUrl': 'https://www.googleapis.com/auth/adwords',
                'httpAuthorizationHeader': 'Bearer %s' % gmb_access_token
            },
            'emailAddress': gmb_email_address,
        },
        # Since this feed's feed items will be managed by AdWords, you must set
        # its origin to ADWORDS.
        'origin': 'ADWORDS'
    }
    # Only include the business_account_identifier if it's specified.
    if business_account_identifier:
        feed['systemFeedGenerationData']['businessAccountIdentifier'] = (
            business_account_identifier
        )
    # Create an operation to add the feed.
    gmb_operations = [{
      'operator': 'ADD',
      'operand': feed
    }]

    gmb_response = client.GetService('FeedService', version='v201702').mutate(
        gmb_operations)
    added_feed = gmb_response['value'][0]
    print('Added GMB feed with ID: %d\n' % added_feed['id'])

    # Add a CustomerFeed that associates the feed with this customer for the
    # LOCATION placeholder type.
    customer_feed = {
        'feedId': added_feed['id'],
        'placeholderTypes': [placeholder_location],
        # Create a matching function that will always evaluate to True.
        'matchingFunction': {
            'operator': 'IDENTITY',
            'lhsOperand': {
                'xsi_type': 'ConstantOperand',
                'type': 'BOOLEAN',
                'booleanValue': True
            }
        }
    }

    customer_feed_operation = {
        'xsi_type': 'CustomerFeedOperation',
        'operator': 'ADD',
        'operand': customer_feed
    }

    customer_feed_service = client.GetService(
        'CustomerFeedService', version='v201702')
    added_customer_feed = None

    i = 0
    while i < max_customer_feed_add_attempts and added_customer_feed is None:
        try:
            added_customer_feed = customer_feed_service.mutate([
                customer_feed_operation])['value'][0]
        except suds.WebFault:
            # Wait using exponential backoff policy
            sleep_seconds = 2 ** i
            print('Attempt %d to add the CustomerFeed was not successful.'
                  'Waiting %d seconds before trying again.\n' % (i, sleep_seconds))

            time.sleep(sleep_seconds)
    i += 1

    if added_customer_feed is None:
        raise errors.GoogleAdsError(
            'Could not create the CustomerFeed after %s attempts. Please retry the '
            'CustomerFeed ADD operation later.' % max_customer_feed_add_attempts)

    print('Added CustomerFeed for feed ID %d and placeholder type %d\n'
          % (added_customer_feed['id'], added_customer_feed['placeholderTypes']))


def create_app_extension(client, campaign_id):
    customer_feed_service = client.GetService('CampaignExtensionSettingService', version='v201702')
    # app_feed_item = {
    #     'xsi_type': 'ApppFeedItem',
    #     'status': 'ENABLED',
    #     'feedType': 'APP',
    #     'appStore': 'GOOGLE_PLAY',
    #     'appId': '',
    #     'appLinkText': 'Download the Cybo app!',
    #     'appUrl': 'https://play.google.com/store/apps/details?id=com.cybo.directory',
    # }
    app_feed_item = {
        'campaignId': campaign_id,
        'extensioniType': 'APP',
        'extensions': [
            {
                ''
            }
        ]
    }
    operation = {
        'operator': 'ADD',
        'xsi_type': 'AppFeedItem',
        'operand': app_feed_item
    }
    customer_feed_service.mutate([operation])


def conversion_tracker(client):  # C.65
    tracker_service = client.GetService('ConversionTrackerService', version='v201702')
    operations = [
        {
            'operator': 'ADD',
            'operand': {
                'name': 'Cybo Signup Conversion Tracker',
                'status': 'ENABLED',
                'category': 'SIGNUP',  # DEFAULT, PAGE_VIEW, PURCHASE, SIGNUP, LEAD, REMARKETING, DOWNLOAD
                'dataDrivenModelStatus': 'AVAILABLE',
                'countingType': 'ONE_PER_CLICK',
            }
        }
    ]
    tracker_service.mutate(operations)


def create_campaign(client, dynamic=False):  # C.10
    # Initialize appropriate services.
    campaign_service = client.GetService('CampaignService', version='v201702')
    budget_service = client.GetService('BudgetService', version='v201702')

    # Create a budget, which can be shared by multiple campaigns.
    budget = {
        'name': 'Eugene Budget',
        'amount': {
            'microAmount': '50000000'
        },
        'deliveryMethod': 'STANDARD'
    }

    budget_operations = [{
        'operator': 'ADD',
        'operand': budget
    }]

    # Add the budget.
    budget_id = budget_service.mutate(budget_operations)['value'][0][
        'budgetId']

    # Construct operations and add campaigns.
    operations = [{
        'operator': 'ADD',
        'operand': {
            'name': 'Eugene Listings',
            # Recommendation: Set the campaign to PAUSED when creating it to
            # stop the ads from immediately serving. Set to ENABLED once you've
            # added targeting and the ads are ready to serve.
            'status': 'PAUSED',
            'advertisingChannelType': 'SEARCH',
            'biddingStrategyConfiguration': {
                'biddingStrategyType': 'MANUAL_CPC',
            },
            'endDate': (datetime.datetime.now() +
                        datetime.timedelta(365)).strftime('%Y%m%d'),
            # Note that only the budgetId is required
            'budget': {
                'budgetId': budget_id
            },
            'networkSetting': {  # C.50
                'targetGoogleSearch': 'true',
                'targetSearchNetwork': 'true',
                'targetContentNetwork': 'false',
                'targetPartnerSearchNetwork': 'false'
            },
            # Optional fields
            'startDate': (datetime.datetime.now() +
                          datetime.timedelta(1)).strftime('%Y%m%d'),
            'adServingOptimizationStatus': 'ROTATE',
            'frequencyCap': {
                'impressions': '5',
                'timeUnit': 'DAY',
                'level': 'ADGROUP'
            },
            'settings': [
                {
                    'xsi_type': 'GeoTargetTypeSetting',
                    'positiveGeoTargetType': 'DONT_CARE',
                    'negativeGeoTargetType': 'DONT_CARE'
                }
            ]
        }
    }]
    if dynamic:
        operations[0]['operand']['settings'].append({  # C.42
            'xsi_type': 'DynamicSearchAdsSetting',
            'domainName': 'brewery.cybo.com',
            'languageCode': 'en'
        })
    campaigns = campaign_service.mutate(operations)

    # Display results.
    campaign_name = campaigns['value'][0]['name']
    campaign_id = campaigns['value'][0]['id']
    print('Campaign with name \'%s\' and id \'%s\' was added.'
          % (campaign_name, campaign_id))
    return campaign_id


def set_campaign_criterion(client, campaign_id, loc_id, location_feed_id=None):
    # Initialize appropriate service.
    campaign_criterion_service = client.GetService(
        'CampaignCriterionService', version='v201702')

    # Create locations. The IDs can be found in the documentation or retrieved
    # with the LocationCriterionService.
    loc = {  # C.20
        'xsi_type': 'Location',
        'id': loc_id
    }
    # eugene library proximity
    prox = {  # C.21.2
        'xsi_type': 'Proximity',
        'geoPoint': {
            'latitudeInMicroDegrees': micro(44.048700),
            'longitudeInMicroDegrees': micro(-123.094887)
        },
        'radiusDistanceUnits': 'MILES',
        'radiusInUnits': '10.00',
        'address': {
            'streetAddress': '100 W 10th Ave',
            'cityName': 'Eugene',
            'provinceName': 'Oregon',
            'provinceCode': 'OR',
            'postalCode': '97402',
            'countryCode': 'US'

        }
    }

    # Create languages. The IDs can be found in the documentation or retrieved
    # with the ConstantDataService.
    english = {  # C.30
        'xsi_type': 'Language',
        'id': '1000'
    }
    spanish = {
        'xsi_type': 'Language',
        'id': '1003'
    }

    # Create a negative campaign criterion operation.
    # negative_campaign_criterion_operand = {
    #     'xsi_type': 'NegativeCampaignCriterion',
    #     'campaignId': campaign_id,
    #     'criterion': {
    #         'xsi_type': 'Keyword',
    #         'matchType': 'BROAD',
    #         'text': 'jupiter cruise'
    #     }
    # }
    criteria = [loc, english, spanish, prox]
    if location_feed_id:
        # Distance targeting. Area of 10 miles around targets above.
        criteria.append({
            'xsi_type': 'LocationGroups',
            'feedId': location_feed_id,
            'matchingFunction': {
                'operator': 'IDENTITY',
                'lhsOperand': [{
                    'xsi_type': 'LocationExtensionOperand',  # C.21.1
                    'radius': {
                        'xsi_type': 'ConstantOperand',
                        'type': 'DOUBLE',
                        'unit': 'MILES',
                        'doubleValue': 10
                    }
                }]
            }
        })
    # Create operations
    operations = []
    for criterion in criteria:
        operations.append({
            'operator': 'ADD',
            'operand': {
                'campaignId': campaign_id,
                'criterion': criterion
            }
        })
    # Add the negative campaign criterion.
    # operations.append({
    #     'operator': 'ADD',
    #     'operand': negative_campaign_criterion_operand
    # })

    # Make the mutate request.
    result = campaign_criterion_service.mutate(operations)

    # Display the resulting campaign criteria.
    # for campaign_criterion in result['value']:
    #     print('Campaign criterion with campaign id \'%s\', criterion id \'%s\', '
    #           'and type \'%s\' was added.'
    #           % (campaign_criterion['campaignId'],
    #              campaign_criterion['criterion']['id'],
    #              campaign_criterion['criterion']['type']))


def set_campaign_platform_bid_modifier(client, campaign_id, platform='desktop'):  # C.14, C.15
    bid_modifier = '1.5'
    campaign_criterion_service = client.GetService('CampaignCriterionService', version='v201702')
    plat = {
          'xsi_type': 'Platform',
          'id': PLATFORMS[platform]
    }
    plat_campaign_criterion = {
        'campaignId': campaign_id,
        'criterion': plat,
        'bidModifier': bid_modifier
    }
    operations = [
        {
            'operator': 'SET',
            'operand': plat_campaign_criterion
        }
    ]
    result = campaign_criterion_service.mutate(operations)
    # for campaign_criterion in result['value']:
    #     print('Campaign criterion with campaign id \'%s\' and criterion id \'%s\' '
    #           'was updated with bid modifier \'%s\'.'
    #           % (campaign_criterion['campaignId'],
    #              campaign_criterion['criterion']['id'],
    #              campaign_criterion['bidModifier']))


def set_campaign_location_bid_modifier(client, campaign_id, location_id=None):  # C.25
    bid_modifier = '1.1'
    campaign_criterion_service = client.GetService('CampaignCriterionService', version='v201702')
    loc = {
        'xsi_type': 'Location',
        'id': location_id
    }
    loc_campaign_criterion = {
        'campaignId': campaign_id,
        'criterion': loc,
        'bidModifier': bid_modifier
    }
    operations = [
        {
            'operator': 'SET',
            'operand': loc_campaign_criterion
        }
    ]
    result = campaign_criterion_service.mutate(operations)


def set_adgroup_bid_modifier(client, campaign_id, adgroup_id, platform='mobile'):  # C.14, C.15
    bid_modifier = '1.6'
    adgroup_modifier_service = client.GetService('AdGroupBidModifierService', version='v201702')
    mobile = {
          'xsi_type': 'Platform',
          'id': PLATFORMS[platform]
    }
    adgroup_modifier = {
        'campaignId': campaign_id,
        'adGroupId': adgroup_id,
        'criterion': mobile,
        'bidModifier': bid_modifier
    }
    operations = [
        {
            'operator': 'SET',
            'operand': adgroup_modifier
        }
    ]
    result = adgroup_modifier_service.mutat(operations)


def create_ad_groups(client, campaign_id, bname):
    # Initialize appropriate service.
    ad_group_service = client.GetService('AdGroupService', version='v201702')

    # Construct operations and add ad groups.
    operations = [{
        'operator': 'ADD',
        'operand': {
            'campaignId': campaign_id,
            'name': bname,
            'status': 'ENABLED',
            'biddingStrategyConfiguration': {
                'bids': [
                    {
                        'xsi_type': 'CpcBid',
                        'bid': {
                            'microAmount': '20000'
                        },
                    }
                ]
            },
            'settings': [
                {
                    # Targeting restriction settings. Depending on the
                    # criterionTypeGroup value, most TargetingSettingDetail only
                    # affect Display campaigns. However, the
                    # USER_INTEREST_AND_LIST value works for RLSA campaigns -
                    # Search campaigns targeting using a remarketing list.
                    'xsi_type': 'TargetingSetting',
                    'details': [
                        # Restricting to serve ads that match your ad group
                        # placements. This is equivalent to choosing
                        # "Target and bid" in the UI.
                        {
                            'xsi_type': 'TargetingSettingDetail',
                            'criterionTypeGroup': 'PLACEMENT',
                            'targetAll': 'false',
                        },
                        # Using your ad group verticals only for bidding. This is
                        # equivalent to choosing "Bid only" in the UI.
                        {
                            'xsi_type': 'TargetingSettingDetail',
                            'criterionTypeGroup': 'VERTICAL',
                            'targetAll': 'true',
                        },
                    ]
                }
            ]
        }
    }]
    try:
        ad_groups = ad_group_service.mutate(operations)
        return ad_groups['value'][0]['id']
    except suds.WebFault as e:
        print('ad group error: %s : %s' % (bname, str(e)))
        if 'EntityCountLimitExceeded' in str(e):
            raise SystemExit("")
        return
    # Display results.
    # for ad_group in ad_groups['value']:
    #     print('Ad group with name \'%s\' and id \'%s\' was added.'
    #           % (ad_group['name'], ad_group['id']))


def create_text_ads(client, ad_group_id, bname, header2, description, finalurl):
    number_of_ads = 1
    # Initialize appropriate service.
    ad_group_ad_service = client.GetService('AdGroupAdService', version='v201702')
    operations = [
        {
            'operator': 'ADD',
            'operand': {
                'xsi_type': 'AdGroupAd',
                'adGroupId': ad_group_id,
                'ad': {
                    'xsi_type': 'ExpandedTextAd',
                    'headlinePart1': bname,
                    'headlinePart2': header2,
                    'description': description,
                    'finalUrls': [finalurl],
                },
                # Optional fields.
                'status': 'ENABLED'
            }
        } for i in range(number_of_ads)
        ]
    try:
        ads = ad_group_ad_service.mutate(operations)
        return ads['value'][0]['ad']['id']
    except suds.WebFault as e:
        print(bname, str(e))
        return
    # Display results.
    # for ad in ads['value']:
    #     print('Ad of type "%s" with id "%d" was added.'
    #           '\n\theadlinePart1: %s\n\theadlinePart2: %s'
    #           % (ad['ad']['Ad.Type'], ad['ad']['id'],
    #              ad['ad']['headlinePart1'], ad['ad']['headlinePart2']))


def create_dynamic_search_ad(client, url):  # C.41
    ad_group_ad_service = client.GetService('AdGroupAdService', version='v201702')
    operations = [
            {
                'operator': 'ADD',
                'operand': {
                    'xsi_type': 'AdGroupAd',
                    'ad': {
                        'xsi_type': 'DynamicSearchAd',
                        'description1': 'Browse for breweries in your area',
                        'description2': 'Craft beer is the best',
                        'displayUrl': url
                    },
                    # Optional fields.
                    'status': 'ENABLED'
                }
            }
        ]
    ads = ad_group_ad_service.mutate(operations)


def create_keywords(client, ad_group_id, keyword, url):
    # Initialize appropriate service.
    ad_group_criterion_service = client.GetService(
        'AdGroupCriterionService', version='v201702')

    # Construct keyword ad group criterion object.
    keyword1 = {
        'xsi_type': 'BiddableAdGroupCriterion',
        'adGroupId': ad_group_id,
        'criterion': {
            'xsi_type': 'Keyword',
            'matchType': 'BROAD',
            'text': keyword
        },
        # These fields are optional.
        'userStatus': 'ENABLED',
        'finalUrls': {
            'urls': ['http://example.com/mars']
        }
    }

    # Construct operations and add ad group criteria.
    operations = [
        {
            'operator': 'ADD',
            'operand': keyword1
        },
    ]
    try:
        ad_group_criteria = ad_group_criterion_service.mutate(
            operations)['value']
    except suds.WebFault as e:
        print('keyword error: %s : %s' % (keyword, str(e)))
    # Display results.
    # for criterion in ad_group_criteria:
    #     print('Keyword ad group criterion with ad group id \'%s\', criterion id '
    #           '\'%s\', text \'%s\', and match type \'%s\' was added.'
    #           % (criterion['adGroupId'], criterion['criterion']['id'],
    #              criterion['criterion']['text'],
    #              criterion['criterion']['matchType']))


def main():
    # Campaign with name 'Eugene Listings' and id '781658398' was added.
    adwords_client = adwords.AdWordsClient.LoadFromStorage('/home/luis/Projects/adwords_api/googleads.yaml')
    get_campaigns(adwords_client)
    # location = 'Eugene, Oregon'
    # cid = create_campaign(adwords_client)
    cid = 781658398  # test
    location_id = 1024453  # Eugene
    set_campaign_criterion(adwords_client, cid, location_id)
    ad_groups = get_adgroups(adwords_client, cid)
    print('ad groups: %d' % len(ad_groups))
    # exit(0)
    listings = {}
    # ['id', 'url', 'isics', 'jdata']
    with open('/home/luis/Projects/adwords_api/eugene_businesses_url.csv') as rfile:
        rcsv = csv.reader(rfile)
        head = next(rcsv)
        c = 0
        for row in tqdm(rcsv):
            # if len(listings) > 4100:
            # if len(listings) >= 10:
            #     break
            j = json.loads(row[3])
            b = j.get('bname')
            if len(b) > 30:
                continue
            if b in ad_groups:
                continue
            s = avg_social_reviews(j)
            if j.get('potential_adult'):
                continue
            isic_id = max(ast.literal_eval(row[2]), key=lambda x: x['select_rank'])['isic_id']
            if isic_id in ADSENSE_UNSUPPORTED_ISICS or isic_id in ADWORDS_UNSUPPORTED_ISICS:
                continue
            if len(row[1]) > 80:
                continue
            u = urlparse(row[1]).path.split('/')
            params = {
                'u': row[1],
                'u1': u[1],
                'u2': u[2],
                'c': ISIC_DICT[isic_id],
                # 'p': p or '',
                's': s,
                'n': j.get('neighborhood', ''),
                'a': j.get('address', '').replace(', Eugene', '') or 'Eugene, Oregon'
            }
            listings[b] = params
            header2, description = create_description(b, params)
            keyword = format_keyword(b)
            # print(header2)
            # print(description)
            # print(b + '->>>>' + keyword)
            # print('-' * 100)
            aid = create_ad_groups(adwords_client, cid, b)
            if not aid:
                continue
            create_text_ads(adwords_client, aid, b, header2, description, params['u'])
            create_keywords(adwords_client, aid, keyword, params['u'])


if __name__ == '__main__':
    main()